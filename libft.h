/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   libft.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sasiedu <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/14 19:10:14 by sasiedu           #+#    #+#             */
/*   Updated: 2016/04/15 16:22:54 by sasiedu          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef LIBFT_H
# define LIBFT_H

#include	<unistd.h>
#include	<stdlib.h>
#include	<fcntl.h>

char	*str1;

typedef	struct	t_map
{
	int	lines;
	int	col;
	char	sq;
	char	obs;
	char	free;
}		t_map;

typedef	struct	t_max
{
	int	m;
	int	r;
	int	c;
}		t_max;

void	read_map(char **argv, int i, int size);
void	ft_putchar(char c);
void	ft_putnbr(int i);
void	ft_solve(int **gd, t_map *map1);
void	ft_max(int **grid, t_map *map);
void	ft_final(int **grid, t_map *map1, t_max *max1);
void	ft_free(t_map *map1, int **grid);
void	ft_file(int i, char **argv);
int	ft_atoi(char *str);
int	ft_map_size(char **argv, int i);
int	ft_min(int a, int b, int c);
int	ft_from_file(char **argv, int i);
int	valid_map();
int	ft_compare(int nb, int k);
int	set_map_val(t_map *map1);
int	**ft_create_grid(t_map *map1);

#endif
