# ************************************************************************** #
#                                                                            #
#                                                        :::      ::::::::   #
#   Makefile                                           :+:      :+:    :+:   #
#                                                    +:+ +:+         +:+     #
#   By: sasiedu <marvin@42.fr>                     +#+  +:+       +#+        #
#                                                +#+#+#+#+#+   +#+           #
#   Created: 2016/04/14 09:10:14 by sasiedu           #+#    #+#             #
#   Updated: 2016/04/15 15:22:54 by sasiedu          ###   ########.fr       #
#                                                                            #
# ************************************************************************** #

NAME = BSQ

SRCS = tools.c checks.c bsq.c main.c

HEADER = libft.h

BINS = tools.o checks.o bsq.o main.o

FLAGS = -Wall -Wextra -Werror

all: $(NAME)

$(NAME):
	@gcc $(FLAGS) $(HEADER) $(SRCS) -c
	@gcc $(FLAGS) -o $(NAME) $(BINS)

clean:
	@rm -f $(BINS)

fclean: clean
	@rm -f $(NAME)

re: fclean all
