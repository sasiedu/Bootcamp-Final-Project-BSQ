/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sasiedu <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/14 19:10:14 by sasiedu           #+#    #+#             */
/*   Updated: 2016/04/15 16:22:54 by sasiedu          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include	"libft.h"
#include	<stdio.h>

int	start;

void	read_map(char **argv, int i, int size)
{
	int	fd;
	int	ret;
	int	j;

	ret = 0;
	fd = open(argv[i], O_RDONLY);
	str1 = (char*)malloc(sizeof(char) * size);
	while (ret == read(fd, str1, size))
		str1[ret] = '\0';
	j = 0;
	while(str1[j] != '\n')
		j++;
	start = j + 1;
}

void	ft_stdin(void)
{
	int	i;
	int	**grid;
	t_map	*map1;
	char	buf;

	str1 = (char*)malloc(sizeof(char) * 40000);
	i = 0;
	while (read(0, &buf, 1))
	{
		str1[i] = buf;
		i++;
	}
	str1[i] = '\0';
	i = 0;
	while (str1[i] != '\n')
		i++;
	start = i + 1;
	map1 = (t_map*)malloc(sizeof(t_map));
	set_map_val(map1);
	grid = ft_create_grid(map1);
	ft_solve(grid, map1);
	ft_free(map1, grid);
}

int	**ft_create_grid(t_map *map1)
{
	int	**grid;
	int	r;
	int	c;

	r = 0;
	grid = (int**)malloc(sizeof(int*) * map1->lines);
	while (r < map1->lines)
	{
		c = 0;
		grid[r] = (int*)malloc(sizeof(int) * map1->col);
		while (c < map1->col)
		{
			if (str1[start] == map1->obs)
				grid[r][c] = 0;
			else
				grid[r][c] = 1;
			c++;
			start++;
		}
		start++;
		r++;
	}
	return (grid);
}

int	main(int argc, char **argv)
{
	int	i;

	i = 1;
	if (argc == 1)
		ft_stdin();
	while (i < argc && argc > 1)
	{
		ft_file(i, argv);
		i++;
	}
	return (0);
}
