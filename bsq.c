/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   bsq.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sasiedu <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/15 19:10:14 by sasiedu           #+#    #+#             */
/*   Updated: 2016/04/15 23:22:54 by sasiedu          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include	"libft.h"

void	ft_solve(int **gd, t_map *map1)
{
	int	r;
	int	c;

	r = 1;
	while (r < map1->lines)
	{
		c = 1;
		while (c < map1->col)
		{
			if (gd[r][c] == 1)
				gd[r][c] = ft_min(gd[r][c - 1], gd[r - 1][c], gd[r -1][c - 1]) + 1;
			else
				gd[r][c] = 0;
			c++;
		}
		r++;
	}
	ft_max(gd, map1);
}

void	ft_max(int **grid, t_map *map1)
{
	int	r;
	int	c;
	t_max	*max1;

	max1 = (t_max*)malloc(sizeof(t_max));
	max1->m = grid[0][0];
	r = 0;
	while (r < map1->lines)
	{
		c = 0;
		while (c < map1->col)
		{
			if (max1->m < grid[r][c])
			{
				max1->m = grid[r][c];
				max1->r = r;
				max1->c = c;	
			}
			c++;
		}
		r++;
	}
	ft_final(grid, map1, max1);
}

void	ft_final(int **grid, t_map *map1, t_max *m)
{
	int	r;
	int	c;

	r = 0;
	while (r < map1->lines)
	{
		c = 0;
		while (c < map1->col)
		{
			if ((r > m->r - m->m && r <= m->r) && (c > m->c - m->m && c <= m->c))
				ft_putchar(map1->sq);
			else if (grid[r][c] == 0)
				ft_putchar(map1->obs);
			else
				ft_putchar(map1->free);
			c++;
		}
		ft_putchar('\n');
		r++;
	}
	free(m);
}

void	ft_free(t_map *map1, int **grid)
{
	int	i;

	i = 0;
	while (i < map1->lines)
	{
		free(grid[i]);
		i++;
	}
	free(map1);
	free(str1);
}
