/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   tools.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sasiedu <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/14 09:10:14 by sasiedu           #+#    #+#             */
/*   Updated: 2016/04/15 15:22:54 by sasiedu          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include	"libft.h"

void	ft_putchar(char c)
{
	write(1, &c, 1);
}

void	ft_putnbr(int i)
{
	if (i > 9)
		ft_putnbr(i / 10);
	ft_putchar(48 + (i % 10));
}

int	ft_atoi(char *str)
{
	int	i;
	int	res;
	int	neg;

	i = 0;
	res = 0;
	neg = 1;
	if (str[i] == '-')
	{
		neg = -1;
		i++;
	}
	while (str[i])
	{
		res = res * 10 + str[i] - '0';
		i++;
	}
	return (neg * res);
}

int	ft_map_size(char **argv, int i)
{
	int	fd;
	int	size;
	char	buf;

	size = 0;
	fd = open(argv[i], O_RDONLY);
	while(read(fd, &buf, 1))
		size++;
	close(fd);
	return (size);
}

int	ft_min(int a, int b, int c)
{
	int	temp;

	temp = a;
	if (temp > b)
		temp = b;
	if (temp > c)
		temp = c;
	return (temp);
}
